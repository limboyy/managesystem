from django.conf.urls import url
from . import views

urlpatterns=[
    url(r'^register/$', views.register),
    url(r'^register_handle/$', views.register_handle),
    url(r'^register_exist/$', views.register_exist),
    url(r'^login/$', views.login),
    url(r'^login_handle/$', views.login_handle),
    url(r'^studentInfo/$', views.studentInfo),
    url(r'^studentSite/$', views.studentSite),
    url(r'^studentResChose/$', views.studentResChose),
    url(r'^studentHonor/$', views.studentHonor),
    url(r'^manageInfo/$', views.manageInfo),
    url(r'^manageSite/$', views.manageSite),
    url(r'^manageOpen/$', views.manageOpen),

    url(r'^teacherInfo/$', views.teacherInfo),
    url(r'^teacherRefuse/$', views.teacherRefuse),
    url(r'^teacherAccept/$', views.teacherAccept),

    url(r'^changePwd/$', views.changePwd),

    url(r'^download/', views.download,name='crm_download'),
    url(r'^stu_chose/', views.stu_chose, name='stu_chose'),
]