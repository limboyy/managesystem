from django.db import models

class MgInfo(models.Model):
    account = models.CharField(max_length=20)
    upwd = models.CharField(max_length=40)
    uname = models.CharField(max_length=40)
    batch = models.IntegerField(default=0)
    class Meta:
        db_table = 'mginfo'

class StuInfo(models.Model):
    account = models.CharField(max_length=20)
    upwd = models.CharField(max_length=40)
    uname = models.CharField(max_length=40)
    volunteer1 = models.IntegerField(default=-1)
    volunteer2 = models.IntegerField(default=-1)
    volunteer3 = models.IntegerField(default=-1)
    choseTeaId = models.IntegerField(default=-1)
    honor = models.TextField()
    major = models.CharField(max_length=40)
    class Meta:
        db_table = 'stuinfo'

class TeaInfo(models.Model):
    account = models.CharField(max_length=20)
    upwd = models.CharField(max_length=40)
    uname = models.CharField(max_length=40)
    vol_math_numb = models.IntegerField(default=0)
    vol_sys_numb = models.IntegerField(default=0)
    vol_wl_numb = models.IntegerField(default=0)
    chose1 = models.CharField(max_length=20)
    chose2 = models.CharField(max_length=20)
    chose3 = models.CharField(max_length=20)
    class Meta:
        db_table = 'teainfo'

# class TeaInfo(models.Model):
#     account = models.CharField(max_length=20)
#     upwd = models.CharField(max_length=40)
#     uname = models.CharField(max_length=40)
#     vol_numb1 = models.IntegerField(default=0)
#     vol_numb2 = models.IntegerField(default=0)
#     vol_numb3 = models.IntegerField(default=0)
#     chose1 = models.CharField(max_length=20)
#     chose2 = models.CharField(max_length=20)
#     chose3 = models.CharField(max_length=20)
#     class Meta:
#         db_table = 'teainfo'