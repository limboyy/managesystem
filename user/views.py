from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponseRedirect, HttpResponse
from .models import *
from hashlib import sha1

# Create your views here.
def register(request):
    return render(request, 'user/register.html')

def register_exist(request):
    account = request.GET.get('uname')
    count1 = StuInfo.objects.filter(account=account).count()
    count2 = TeaInfo.objects.filter(account=account).count()
    count3 = MgInfo.objects.filter(account=account).count()
    count = count1 + count2 + count3
    return JsonResponse({'count': count})

def register_handle(request):
    post = request.POST
    account = post.get('user_name')
    pwd = post.get('pwd')
    cpwd = post.get('cpwd')
    uname = post.get('uname')
    major = post.get('major')
    if pwd != cpwd:
        return redirect('/user/register/')
    s1 = sha1()
    s1.update((pwd.encode("utf-8")))
    upwd = s1.hexdigest()
    user = StuInfo()
    user.uname = uname
    user.upwd = upwd
    user.account = account
    user.major = major
    user.save()
    return JsonResponse({'res': 'True'})

def login(request):
    account = request.COOKIES.get('account', '')
    context = {'title': '用户登陆', 'error_name': 0, 'error_pwd': 0, 'account':account}
    return render(request, 'user/login.html', context)

def login_handle(request):
    post = request.POST
    account = post.get('username')
    upwd = post.get('pwd')
    jizhu = post.get('jizhu', 0)
    stu = StuInfo.objects.filter(account=account)
    tea = TeaInfo.objects.filter(account=account)
    mg = MgInfo.objects.filter(account=account)
    if stu.count() == 1 or tea.count() == 1 or mg.count() == 1:
        s1 = sha1()
        s1.update(upwd.encode("utf-8"))

        if tea.count() == 1:
            user = tea[0]
            red = HttpResponseRedirect('/user/teacherInfo/')
        elif mg.count() == 1:
            user = mg[0]
            red = HttpResponseRedirect('/user/manageInfo/')
        else:
            user = stu[0]
            red = HttpResponseRedirect('/user/studentInfo/')
        if s1.hexdigest() == user.upwd:
            if jizhu != 0:
                red.set_cookie('uname', account)
            else:
                red.set_cookie('uname', '', max_age=-1)
            request.session['user_id'] = user.id
            request.session['user_name'] = account
            return red
        else:
            context = {'title': '用户登陆', 'error_name':0, 'error_pwd':1, 'uname':account, 'upwd':upwd}
            return render(request, 'user/login.html', context)
    else:
        context = {'title': '用户登陆', 'error_name': 1, 'error_pwd': 0, 'uname': account, 'upwd': upwd}
        return render(request, 'user/login.html', context)



def manageInfo(request):
    uname = MgInfo.objects.get(id=request.session['user_id']).uname
    context = {
        'title': '管理员分配名额', 'uname': uname
    }
    return render(request, 'user/manageInfo.html', context)

def manageOpen(request):
    type = request.POST.get("type")
    mg_info = MgInfo.objects.all()[0]
    mg_info.batch = type
    mg_info.save()
    return JsonResponse({'res': 'True'})

def manageSite(request):
    if request.method == 'POST':
        # tea_info = TeaInfo()
        tea_name = request.POST.get("tea_name")
        # num = request.POST.get("num")
        num_math = request.POST.get("num_math", 0)
        num_sys = request.POST.get("num_sys", 0)
        num_wuli = request.POST.get("num_wuli", 0)
        pwd = request.POST.get("pwd", 0)
        # type = request.POS T.get("type")
        tea_name_pin = request.POST.get("tea_name_pin")
        tea_res = TeaInfo.objects.filter(account=tea_name_pin)
        if len(tea_res) == 1:
            tea_res[0].vol_math_numb = num_math
            tea_res[0].vol_sys_numb = num_sys
            tea_res[0].vol_wl_numb = num_wuli
            tea_res[0].save()
        else:
            tea_info = TeaInfo()
            tea_info.account = tea_name_pin
            s1 = sha1()
            s1.update((pwd.encode("utf-8")))
            upwd = s1.hexdigest()
            tea_info.upwd = upwd
            tea_info.uname = tea_name
            # tea_info.vol_numb = num
            tea_info.vol_math_numb = num_math
            tea_info.vol_sys_numb = num_sys
            tea_info.vol_wl_numb = num_wuli
            tea_info.save()
        teachers = TeaInfo.objects.all()
        teacher_info = []
        for tea in teachers:
            teacher_info.append([tea.uname, tea.vol_math_numb, tea.vol_sys_numb, tea.vol_wl_numb, tea.chose1, tea.chose2, tea.chose3])
        return JsonResponse({'res': 'True', "res_title": '分配成功', 'teachers': teacher_info})
    else:
        teachers = TeaInfo.objects.all()
        teacher_info = []
        for tea in teachers:
            tea_data_1 = []
            tea_data_2 = []
            tea_data_3 = []
            for i in tea.chose1.split(','):
                if i != '':
                    tea_data_1.append(StuInfo.objects.get(id=int(i)).uname)
            for i in tea.chose2.split(','):
                if i != '':
                    tea_data_2.append(StuInfo.objects.get(id=int(i)).uname)
            for i in tea.chose3.split(','):
                if i != '':
                    tea_data_3.append(StuInfo.objects.get(id=int(i)).uname)
            teacher_info.append([tea.uname, tea.vol_math_numb, tea.vol_sys_numb,tea.vol_wl_numb, tea_data_1, tea_data_2, tea_data_3])
        # print(teacher_info)
            # teacher_info.append([tea.uname, tea.vol_numb1, tea.vol_numb2, tea.vol_numb3])
        return JsonResponse({'res': 'True', "res_title": '分配成功', 'teachers': teacher_info})

def studentResChose(request):
    stu = StuInfo.objects.get(id=request.session['user_id'])
    volunteer1 = ''
    volunteer3 = ''
    volunteer2 = ''
    tea2 = TeaInfo.objects.filter(id=stu.volunteer2)
    tea3 = TeaInfo.objects.filter(id=stu.volunteer3)
    tea1 = TeaInfo.objects.filter(id=stu.volunteer1)
    if len(tea1) == 1:
        volunteer1 = tea1[0].uname
    if len(tea2) == 1:
        volunteer2 = tea2[0].uname
    if len(tea3) == 1:
        volunteer3 = tea3[0].uname
    return JsonResponse({'res': 'True', 'tea_chose': [volunteer1, volunteer2, volunteer3]})

def studentInfo(request):
    stu = StuInfo.objects.get(id=request.session['user_id'])
    volunteer1 = ''
    volunteer3 = ''
    volunteer2 = ''
    tea2 = TeaInfo.objects.filter(id=stu.volunteer2)
    tea3 = TeaInfo.objects.filter(id=stu.volunteer3)
    tea1 = TeaInfo.objects.filter(id=stu.volunteer1)
    if len(tea1) == 1:
        volunteer1 = tea1[0].uname
    if len(tea2) == 1:
        volunteer2 = tea2[0].uname
    if len(tea3) == 1:
        volunteer3 = tea3[0].uname
    teachers = TeaInfo.objects.all()
    # teachers_not_chose = []
    # not_chose_teachers = ['冯春宝', '张益', '朱家骥', '王良晨', '杨春德', '张莉敏', '朱伟', '田娅', '虞继敏', '邓志颖', '方长杰']
    # for tes in teachers:
    #     uname = tes.uname
    #     if uname in not_chose_teachers:
    #         teachers_not_chose.append({'uname': uname, 'id': tes.id})
    # teachers = teachers_not_chose

    context = {
        'title': '学生志愿填报', 'uname': stu.uname, 'teachers': teachers, 'volunteer1': volunteer1,
        'volunteer2':volunteer2, 'volunteer3':volunteer3, 'honor': stu.honor,
    }
    return render(request, 'user/studentInfo.html', context)

def studentSite(request):
    stu_info = StuInfo.objects.get(id=request.session['user_id'])
    if request.method == 'POST':
        zhiyuan = request.POST.get("zhiyuan")
        teacher_id = int(request.POST.get("teacher"))
        stu_info_major = stu_info.major
        tea_info = TeaInfo.objects.get(id=teacher_id)
        if stu_info_major == '系统科学专业':
            if tea_info.vol_sys_numb == 0:
                return JsonResponse({'res': 'False', "res_title": "该老师没有系统科学的名额"})
        if stu_info_major == '数学专业':
            if tea_info.vol_math_numb == 0:
                return JsonResponse({'res': 'False', "res_title": "该老师没有数学的名额"})
        if stu_info_major == '物理专业':
            if tea_info.vol_wl_numb == 0:
                return JsonResponse({'res': 'False', "res_title": "该老师没有物理的名额"})

        if zhiyuan == '1':
            stu_info_volunteer1 = stu_info.volunteer1
            # print(stu_info_volunteer1)
            if stu_info_volunteer1 > -1 or stu_info_volunteer1 == -2:
                return JsonResponse({'res': 'False', "res_title": "一志愿已经选择过了"})
            if stu_info.volunteer2 != teacher_id and stu_info.volunteer3 != teacher_id:
                stu_info.volunteer1 = teacher_id
            else:
                return JsonResponse({'res': 'False', "res_title": "已经选择了该老师"})
        elif zhiyuan == '2':
            stu_info_volunteer2 = stu_info.volunteer2
            if stu_info_volunteer2 > -1 or stu_info_volunteer2== -2:
                return JsonResponse({'res': 'False', "res_title": "二志愿已经选择过了"})
            if stu_info.volunteer1 != teacher_id and stu_info.volunteer3 != teacher_id:
                stu_info.volunteer2 = teacher_id
            else:
                return JsonResponse({'res': 'False', "res_title": "已经选择了该老师"})
        else:
            stu_info_volunteer3 = stu_info.volunteer3
            if stu_info_volunteer3 > -1 or stu_info_volunteer3== -2:
                return JsonResponse({'res': 'False', "res_title": "三志愿已经选择过了"})
            if stu_info.volunteer1 != teacher_id and stu_info.volunteer2 != teacher_id:
                stu_info.volunteer3 = teacher_id
            else:
                return JsonResponse({'res': 'False', "res_title": "已经选择了该老师"})
        stu_info.save()
        return JsonResponse({'res': 'True', "res_title": "选择成功"})

def studentHonor(request):
    import os
    stu = StuInfo.objects.get(id=request.session['user_id'])
    if request.method == "POST":
        myfile = request.FILES.get("inputfile")
        if not myfile:
            return HttpResponse("没有文件！请返回")
        BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        jianli_name = stu.account + "." + (myfile.name).split('.')[-1]
        save_path = os.path.join(os.path.join(BASE_DIR, r"static/jianli/"), jianli_name)
        # print(save_path)
        destination = open(save_path, 'wb+')
        for chunk in myfile.chunks():
            destination.write(chunk)
        destination.close()
        if stu.honor != '':
            return HttpResponse("已经上传过了！请返回")
        stu.honor = save_path
        stu.save()
        return HttpResponse("上传成功！请返回")

from django.http import FileResponse
def download(request):
    filename = request.GET.get("filename")
    if not filename:
        return HttpResponse("该生没有上传简历！")
    file=open(filename,'rb')
    file_type = filename.split(".")[-1]
    response =FileResponse(file)
    response['Content-Type']='application/octet-stream'

    response['Content-Disposition']='attachment;filename="curriculum_vitae.%s"'% file_type
    return response



def teacherAccept(request):
    user_id = request.session['user_id']
    stu_id = request.POST.get('stu_id')
    type = request.POST.get('type')
    tea_info = TeaInfo.objects.get(id=user_id)
    chose1 = tea_info.chose1
    chose2 = tea_info.chose2
    chose3 = tea_info.chose3
    chose = chose1+chose2+chose3
    stu_sys_list = []
    stu_math_list = []
    stu_wl_list = []
    if stu_id in chose.split(','):
        return JsonResponse({'static': 'True', 'title': "您已经选择了该学生"})
    for c in chose.split(','):
        if c != '':
            stu_info = StuInfo.objects.get(id=int(c))
            stu_info_id = stu_info.id
            stu_info_uname = stu_info.uname
            stu_info_major = stu_info.major
            if stu_info_major == "系统科学专业":
                stu_sys_list.append({'id': stu_info_id, 'uname': stu_info_uname, 'major': stu_info_major})
            elif stu_info_major == "数学专业":
                stu_math_list.append({'id': stu_info_id, 'uname': stu_info_uname, 'major': stu_info_major})
            else:
                stu_wl_list.append({'id': stu_info_id, 'uname': stu_info_uname, 'major': stu_info_major})
    chose_stu_info = StuInfo.objects.get(id=stu_id)
    chose_stu_info_major = chose_stu_info.major
    if chose_stu_info_major == "系统科学专业":
        if len(stu_sys_list) >= tea_info.vol_sys_numb:
            return JsonResponse({'static': 'True', 'title': "超过了系统科学专业的名额，不能再选择了"})
        if type == '1':
            tea_info.chose1 = chose1 + ',' + stu_id
        elif type == '2':
            tea_info.chose2 = chose2 + ',' + stu_id
        elif type == '3':
            tea_info.chose3 = chose3 + ',' + stu_id
        chose_stu_info.choseTeaId = user_id
        chose_stu_info.save()
        tea_info.save()
        return JsonResponse({'static': 'True', 'title': "选择成功"})
    elif chose_stu_info_major == "物理专业":
        if len(stu_wl_list) >= tea_info.vol_wl_numb:
            return JsonResponse({'static': 'True', 'title': "超过了物理专业的名额，不能再选择了"})
        if type == '1':
            tea_info.chose1 = chose1 + ',' + stu_id
        elif type == '2':
            tea_info.chose2 = chose2 + ',' + stu_id
        elif type == '3':
            tea_info.chose3 = chose3 + ',' + stu_id
        chose_stu_info.choseTeaId = user_id
        chose_stu_info.save()
        tea_info.save()
        return JsonResponse({'static': 'True', 'title': "选择成功"})
    else:
        if len(stu_math_list) >= tea_info.vol_math_numb:
            return JsonResponse({'static': 'True', 'title': "超过了数学专业的名额，不能再选择了"})
        if type == '1':
            tea_info.chose1 = chose1 + ',' + stu_id
        elif type == '2':
            tea_info.chose2 = chose2 + ',' + stu_id
        elif type == '3':
            tea_info.chose3 = chose3 + ',' + stu_id
        chose_stu_info.choseTeaId = user_id
        chose_stu_info.save()
        tea_info.save()
        return JsonResponse({'static': 'True', 'title': "选择成功"})

def teacherRefuse(request):
    user_id = request.session['user_id']
    stu_id = request.POST.get('stu_id')
    type = request.POST.get('type')
    tea_info = TeaInfo.objects.get(id=user_id)
    stu_info = StuInfo.objects.get(id=stu_id)
    chose1 = tea_info.chose1
    chose2 = tea_info.chose2
    chose3 = tea_info.chose3
    chose = (chose1+chose2+chose3).split(',')
    teaChose_sys_names = []
    teaChose_math_names = []
    teaChose_wuli_names = []
    for c in chose:
        if c != "":
            stu_Info = StuInfo.objects.filter(id=c)[0]
            stu_name = stu_Info.uname
            stu_major = stu_Info.major
            stu_jianli = stu_Info.honor
            if stu_major == "系统科学专业":
                teaChose_sys_names.append({'stu_name': stu_name, 'stu_jianli': stu_jianli, 'stu_major': stu_major})
            elif stu_major == "数学专业":
                teaChose_math_names.append({'stu_name': stu_name, 'stu_jianli': stu_jianli, 'stu_major': stu_major})
            elif stu_major == "物理专业":
                teaChose_wuli_names.append({'stu_name': stu_name, 'stu_jianli': stu_jianli, 'stu_major': stu_major})

    tea_info_vol_numb_math = tea_info.vol_math_numb - len(teaChose_math_names)
    tea_info_vol_numb_sys = tea_info.vol_sys_numb - len(teaChose_sys_names)
    tea_info_vol_numb_wuli = tea_info.vol_wl_numb - len(teaChose_wuli_names)
    num_i_math = 0
    num_i_sys = 0
    num_i_wuli = 0
    if stu_id in chose:
        return JsonResponse({'static': 'True', 'title': "您已经选择了他，不能拒绝！"})
    if type == '1':
        stu_tea_data = StuInfo.objects.filter(volunteer1=user_id)
        for stu in stu_tea_data:
            stu_choseTeaId = stu.choseTeaId
            stu_major = stu.major

            if stu_choseTeaId == -1 and stu_major == "数学专业":
                num_i_math += 1
            elif stu_choseTeaId == -1 and stu_major == "系统科学专业":
                num_i_sys += 1
            elif stu_choseTeaId == -1 and stu_major == "物理专业":
                num_i_wuli += 1
        stu_info_major = stu_info.major
        # if stu_info_major == "数学专业":
        #     if num_i_math <= tea_info_vol_numb_math:
        #         return JsonResponse({'static': 'True', 'title': "数学方向分配给你的名额大于报考人数，不能拒绝！"})
        # elif stu_info_major == "系统科学专业":
        #     if num_i_sys <= tea_info_vol_numb_sys:
        #         return JsonResponse({'static': 'True', 'title': "系统科学方向分配给你的名额大于报考人数，不能拒绝！"})
        # elif stu_info_major == "物理专业":
        #     if num_i_wuli <= tea_info_vol_numb_wuli:
        #         return JsonResponse({'static': 'True', 'title': "物理方向分配给你的名额大于报考人数，不能拒绝！"})
        stu_info.volunteer1 = -2
    elif type == '2':
        stu_tea_data = StuInfo.objects.filter(volunteer2=user_id)
        for stu in stu_tea_data:
            stu_choseTeaId = stu.choseTeaId
            stu_major = stu.major
            if stu_choseTeaId == -1 and stu_major == "数学专业":
                num_i_math += 1
            elif stu_choseTeaId == -1 and stu_major == "系统科学专业":
                num_i_sys += 1
            elif stu_choseTeaId == -1 and stu_major == "物理专业":
                num_i_wuli += 1
        stu_info_major = stu_info.major
        # if stu_info_major == "数学专业":
        #     if num_i_math <= tea_info_vol_numb_math:
        #         return JsonResponse({'static': 'True', 'title': "数学方向分配给你的名额大于报考人数，不能拒绝！"})
        # elif stu_info_major == "系统科学专业":
        #     if num_i_sys <= tea_info_vol_numb_sys:
        #         return JsonResponse({'static': 'True', 'title': "系统科学方向分配给你的名额大于报考人数，不能拒绝！"})
        # elif stu_info_major == "物理专业":
        #     if num_i_wuli <= tea_info_vol_numb_wuli:
        #         return JsonResponse({'static': 'True', 'title': "物理方向分配给你的名额大于报考人数，不能拒绝！"})
        stu_info.volunteer2 = -2
    else:
        stu_tea_data = StuInfo.objects.filter(volunteer3=user_id)
        for stu in stu_tea_data:
            stu_choseTeaId = stu.choseTeaId
            stu_major = stu.major
            if stu_choseTeaId == -1 and stu_major == "数学专业":
                num_i_math += 1
            elif stu_choseTeaId == -1 and stu_major == "系统科学专业":
                num_i_sys += 1
            elif stu_choseTeaId == -1 and stu_major == "物理专业":
                num_i_wuli += 1
        stu_info_major = stu_info.major
        # if stu_info_major == "数学专业":
        #     if num_i_math <= tea_info_vol_numb_math:
        #         return JsonResponse({'static': 'True', 'title': "数学方向分配给你的名额大于报考人数，不能拒绝！"})
        # elif stu_info_major == "系统科学专业":
        #     if num_i_sys <= tea_info_vol_numb_sys:
        #         return JsonResponse({'static': 'True', 'title': "系统科学方向分配给你的名额大于报考人数，不能拒绝！"})
        # elif stu_info_major == "物理专业":
        #     if num_i_wuli <= tea_info_vol_numb_wuli:
        #         return JsonResponse({'static': 'True', 'title': "物理方向分配给你的名额大于报考人数，不能拒绝！"})
        stu_info.volunteer3 = -2
    stu_info.save()
    return JsonResponse({'static': 'True', 'title':"拒绝成功"})

def teacherInfo(request):
    user_id = request.session['user_id']
    tea_info = TeaInfo.objects.get(id=user_id)
    batch = MgInfo.objects.all()[0].batch
    vol_math_numb = tea_info.vol_math_numb #数学专业的人数分配名额
    vol_sys_numb = tea_info.vol_sys_numb #系统科学的人数分配名额
    vol_wl_numb = tea_info.vol_wl_numb #物理的人数分配名额
    #导师选择的学生名单
    teaChose_math_names = []
    teaChose_sys_names = []
    teaChose_wuli_names = []
    context = {'uname': tea_info.uname, 'type': batch, 'title': '教师系统', 'vol_math_numb':vol_math_numb, 'vol_sys_numb':vol_sys_numb, 'vol_wl_numb': vol_wl_numb}
    if batch == 0:
        return render(request, 'user/teacherInfo.html', context)
    for i in (tea_info.chose1 + tea_info.chose2 + tea_info.chose3).split(','):
        if i != '':
            stu_Info = StuInfo.objects.filter(id=i)[0]
            stu_name = stu_Info.uname
            stu_major = stu_Info.major
            stu_jianli = stu_Info.honor
            if stu_major == "系统科学专业":
                teaChose_sys_names.append({'stu_name': stu_name, 'stu_jianli': stu_jianli, 'stu_major': stu_major})
            elif stu_major == "数学专业":
                teaChose_math_names.append({'stu_name': stu_name, 'stu_jianli': stu_jianli, 'stu_major': stu_major})
            else:
                teaChose_wuli_names.append({'stu_name': stu_name, 'stu_jianli': stu_jianli, 'stu_major': stu_major})
    wuli_tea_list = [
        '13883829008','15825951082','18983785925','18323223205','13368374575','18523476334',
        '15923356075','15723159826','13996006875','13696461221','18680785914','15178806110',
        '15330206925','13667614680','13594003026','15023308856','19923937521','13883608155',
        '15736285039','18323012292','15002358973','15340514021', '13618333412'
    ] #物理老师的名单
    if tea_info.account not in wuli_tea_list:
        '''如果老师不是物理方向的'''
        if batch == 1:
            stus_tea = StuInfo.objects.filter(volunteer1=user_id)
        elif batch == 2:
            stus_tea = StuInfo.objects.filter(volunteer2=user_id)
        else:
            stus_tea = StuInfo.objects.filter(volunteer3=user_id)

        #导师选择的学生
        # chose1 = tea_info.chose1
        # chose2 = tea_info.chose2
        # chose3 = tea_info.chose3

        # 选择该导师的学生
        stu_tea_sys_list = []
        stu_tea_math_list = []
        for stu in stus_tea:
            stu_id = stu.id
            stu_uname = stu.uname
            stu_major = stu.major
            stu_honor = stu.honor
            stu_choseTeaId = stu.choseTeaId
            if stu_major == "系统科学专业" and stu_choseTeaId == -1:
                stu_tea_sys_list.append({'id':stu_id, 'uname':stu_uname, 'honor': stu_honor, 'stu_major': stu_major})
            elif stu_major == "数学专业" and stu_choseTeaId == -1:
                stu_tea_math_list.append({'id': stu_id, 'uname': stu_uname, 'honor': stu_honor, 'stu_major': stu_major})
        stu_tea_sys_list.extend(stu_tea_math_list)
        context['stu_tea_name'] = stu_tea_sys_list
        tea_info = TeaInfo.objects.get(id=user_id)
        teaChose_sys_names = []
        teaChose_math_names = []
        for i in (tea_info.chose1 + tea_info.chose2 + tea_info.chose3).split(','):
            if i != '':
                stu_Info = StuInfo.objects.filter(id=i)[0]
                stu_name = stu_Info.uname
                stu_major = stu_Info.major
                stu_jianli = stu_Info.honor
                if stu_major == "系统科学专业":
                    teaChose_sys_names.append({'stu_name': stu_name, 'stu_jianli': stu_jianli, 'stu_major': stu_major})
                else:
                    teaChose_math_names.append({'stu_name': stu_name, 'stu_jianli': stu_jianli, 'stu_major': stu_major})
        teaChose_sys_names.extend(teaChose_math_names)
        context['stu_names'] = teaChose_sys_names
        if batch == 1:
            stus_tea = StuInfo.objects.filter(volunteer1=user_id)
        elif batch == 2:
            stus_tea = StuInfo.objects.filter(volunteer2=user_id)
        else:
            stus_tea = StuInfo.objects.filter(volunteer3=user_id)
        stu_tea_list = []
        for stu in stus_tea:
            stu_id = stu.id
            stu_uname = stu.uname
            stu_major = stu.major
            stu_honor = stu.honor
            stu_choseTeaId = stu.choseTeaId
            if stu_choseTeaId == -1:
                stu_tea_list.append({'id':stu_id, 'uname':stu_uname, 'honor': stu_honor, 'stu_major': stu_major})
        context['stu_tea_name'] = stu_tea_list
        return render(request, 'user/teacherInfo.html', context)
    else:

        tea_info = TeaInfo.objects.get(id=user_id)
        teaChose_wuli_names = []
        # teaChose_math_names = []
        for i in (tea_info.chose1 + tea_info.chose2 + tea_info.chose3).split(','):
            if i != '':
                stu_Info = StuInfo.objects.filter(id=i)[0]
                stu_name = stu_Info.uname
                stu_major = stu_Info.major
                stu_jianli = stu_Info.honor
                if stu_major == "物理专业":
                    teaChose_wuli_names.append({'stu_name': stu_name, 'stu_jianli': stu_jianli, 'stu_major': stu_major})
                # else:
                #     teaChose_math_names.append({'stu_name': stu_name, 'stu_jianli': stu_jianli, 'stu_major': stu_major})
        # teaChose_sys_names.extend(teaChose_math_names)
        context['stu_names'] = teaChose_wuli_names

        if batch == 1:
            stus_tea = StuInfo.objects.filter(volunteer1=user_id)
        elif batch == 2:
            stus_tea = StuInfo.objects.filter(volunteer2=user_id)
        else:
            stus_tea = StuInfo.objects.filter(volunteer3=user_id)
        stu_tea_list = []
        for stu in stus_tea:
            stu_id = stu.id
            stu_uname = stu.uname
            stu_major = stu.major
            stu_honor = stu.honor
            stu_choseTeaId = stu.choseTeaId
            if stu_major == "物理专业" and stu_choseTeaId == -1:
                stu_tea_list.append({'id':stu_id, 'uname':stu_uname, 'honor': stu_honor, 'stu_major': stu_major})
        context['stu_tea_name'] = stu_tea_list
        return render(request, 'user/teacherInfo.html', context)

def changePwd(request):
    user_id = request.session['user_id']
    # tea_info = TeaInfo.objects.get(id=user_id)
    old_pwd = request.POST.get('old_pwd')
    new_pwd = request.POST.get('new_pwd')
    cnew_pwd = request.POST.get('cnew_pwd')
    title = request.POST.get('title')
    if title == '学生志愿填报':
        user_info = StuInfo.objects.get(id=user_id)
    elif title == '教师系统':
        user_info = TeaInfo.objects.get(id=user_id)
    else:
        user_info = MgInfo.objects.get(id=user_id)
    s1 = sha1()
    s1.update((old_pwd.encode("utf-8")))
    old_pwd = s1.hexdigest()
    if user_info.upwd != old_pwd:
        return JsonResponse({'static': 'False', 'title_eer':"原密码错误"})
    s1 = sha1()
    s1.update((new_pwd.encode("utf-8")))
    new_pwd = s1.hexdigest()
    user_info.upwd = new_pwd
    user_info.save()
    return JsonResponse({'static': 'True', 'title_eer':"修改成功"})


def stu_chose(request):
    res = StuInfo.objects.all()
    data = []
    for i in res:

        name = i.uname
        major = i.major
        volunteer1 = i.volunteer1
        volunteer2 = i.volunteer2
        volunteer3 = i.volunteer3
        choseTeaId = i.choseTeaId
        honor = i.honor

        if choseTeaId != -1:
            choseTeaId = TeaInfo.objects.get(id=choseTeaId).uname
        else:
            choseTeaId = ""
        if volunteer1 == -1:
            volunteer1 = "该生还没有选导师"
        elif volunteer1 == -2:
            volunteer1 = "该生已经被拒绝"
        else:
            volunteer1 = TeaInfo.objects.get(id=i.volunteer1).uname

        if volunteer2 == -1:
            volunteer2 = "该生还没有选导师"
        elif volunteer2 == -2:
            volunteer2 = "该生已经被拒绝"
        else:
            volunteer2 = TeaInfo.objects.get(id=i.volunteer2).uname

        if volunteer3 == -1:
            volunteer3 = "该生还没有选导师"
        elif volunteer3 == -2:
            volunteer3 = "该生已经被拒绝"
        else:
            volunteer3 = TeaInfo.objects.get(id=i.volunteer3).uname

        data.append({"name": name, 'major': major, 'volunteer1': volunteer1, 'volunteer2': volunteer2, 'volunteer3': volunteer3, 'honor': honor, 'choseTeaId': choseTeaId})

    context = {'res': data}
    return render(request, 'user/stu_zhiyuan.html', context)