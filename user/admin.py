from django.contrib import admin
from .models import MgInfo, StuInfo, TeaInfo

admin.site.register(MgInfo)
admin.site.register(StuInfo)
admin.site.register(TeaInfo)
# Register your models here.
