from django.db import models

# Create your models here.
'''
1.Email
2.姓名
3.邮箱
4.建议

'''

class Suggest(models.Model):
    uName = models.CharField(max_length=20)
    uEmail = models.CharField(max_length=200)
    uSuggest = models.TextField(max_length=2000)
    class Meta:
        db_table = "suggest"

class Love(models.Model):
    '''
    1.分类
    2.路径
    '''
    classify = models.IntegerField()
    path = models.CharField(max_length=100)
    class Meta:
        db_table = 'love'